﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordinal;

        public Card(string name, string seed, int ordinal)
        {
            this.name = name;
            this.ordinal = ordinal;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        public string Seed
        {
            get { return this.seed; }
        }

        public string Name
        {
            get { return this.name; }
        }

        public int Ordinal
        {
            get { return ordinal; }
        }

        public override string ToString()
        {
            return $"{GetType().Name}(Name={this.name}, Seed={this.seed}, Ordinal={this.ordinal})";
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Card c = (Card)obj;
                return (seed == c.seed) && (name == c.name) && (ordinal == c.ordinal);
            }
        }

        public override int GetHashCode()
        {
            return (seed.GetHashCode() - name.GetHashCode()) ^ ordinal;
        }
    }

}